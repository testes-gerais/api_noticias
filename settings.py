import psycopg2
from decouple import config
from flask_sqlalchemy import SQLAlchemy
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

user = config('DB_USER')
psswd = config('DB_PSSWD')
name = config('DB_NAME')
port = config('DB_PORT')
host = config('DB_HOST')

DB_URL = f'postgresql+psycopg2://{user}:{psswd}@db:{port}/{name}'

db = SQLAlchemy()

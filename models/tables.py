from settings import db
from sqlalchemy_utils import database_exists, create_database, drop_database

class Noticia(db.Model):
    __tablename__ = 'noticia'

    id_noticia = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title_noticia = db.Column(db.String)
    text_noticia = db.Column(db.String)
    id_autor = db.Column(db.Integer, db.ForeignKey('autor.id_autor'))

    def __init__(self, id_noticia, title_noticia, text_noticia, id_autor):
        self.id_noticia = id_noticia
        self.title_noticia = title_noticia
        self.text_noticia = text_noticia
        self.id_autor = id_autor

    def save_noticia(self):
        db.session.add(self)
        db.session.commit()

    def update_noticia(self):
        db.session.commit()

    def delete_noticia(self, data):
        db.session.delete(data)
        db.session.commit()

    def jsonNoticia(self):
        return dict(id_noticia=self.id_noticia, title_noticia=self.title_noticia, text_noticia=self.text_noticia,
                    id_autor=self.id_autor)

    @classmethod
    def find_by_id_notcia(cls, _id):
        return cls.query.filter_by(id_noticia=_id).first()

    @classmethod
    def find_by_title(cls, _title):
        return cls.query.filter_by(title_noticia=_title).first()

    @classmethod
    def find_by_title_all(cls, _title):
        return cls.query.filter_by(title_noticia=_title).all()

    @classmethod
    def find_by_text(cls, _text):
        return cls.query.filter_by(text_noticia=_text).all()

    @classmethod
    def find_by_id_autor(cls, _id):
        return cls.query.filter_by(id_autor=_id).first()


class Autor(db.Model):
    __tablename__ = 'autor'

    id_autor = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name_autor = db.Column(db.String)
    id_noticia = db.relationship("Noticia", backref="autor")

    def __init__(self, id_autor, name_autor):
        self.id_autor = id_autor
        self.name_autor = name_autor

    def save_author(self):
        db.session.add(self)
        db.session.commit()

    def update_author(self):
        db.session.commit()

    def delete_author(self, data):
        db.session.delete(data)
        db.session.commit()

    def jsonAutor(self):
        return dict(id_autor=self.id_autor, name_autor=self.name_autor)

    def jsonNoticia(self):
        return Noticia.jsonNoticia(self)

    @classmethod
    def find_by_name(cls, _name):
        return cls.query.filter_by(name_autor=_name).first()

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id_autor=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.filter_by().all()
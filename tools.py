import logging
from flask import make_response

def logs():
    logging.basicConfig(
        format="[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s: %(message)s",
        datefmt="%d/%m/%Y %I:%M:%S %p",
        level=logging.INFO,
    )

    logger = logging.getLogger(__name__)

    return logger

def check_keys(obj):
    list_keys = ['title_noticia', 'text_noticia', 'autor']
    result = [check for check in list_keys if check not in obj.keys()]

    return result

def service_error(mensagem, status):
    erro = dict(error=mensagem, status=status)

    raise Exception(erro)

def mount_error(e, stacktrace):
    erro = dict(Erro=f"{e.__class__.__name__}:{e}", Stacktrace=stacktrace)
    logger.error(erro)
    status = e.args[0]['status'] if 'status' in e.args[0] else 400

    return make_response(erro, status)

def service_update_list_noticia(data_noticia, autor_name=False):
    noticia_list = list()

    for noticia in data_noticia:
        autor = noticia.autor.name_autor if not autor_name else autor_name
        data = noticia.jsonNoticia()
        data.update(autor=autor)
        noticia_list.append(data)

    return noticia_list
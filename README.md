# Instruções de uso da API Noticias

### Pré Requisitos:
    *Python3.7
    *Docker
    *Docker-compose
    *Postman

#### Baixar o projeto:

```
git clone https://gitlab.com/testes-gerais/api_noticias.git
```

#### Observações

    1 - Criar um arquivo ".env" com as variaveis indicadas na wiki do projeto.
    2 - Esse arquivo deve ser copiado para o diretório do projeto, como mostra imagem anexada na wiki do projeto.
    3 - Link da pagina na wiki: https://gitlab.com/testes-gerais/api_noticias/-/wikis/Configuracoes

#### Como executar o serviço:

* No diretorio do projeto execute:

```
docker_compose up
```

### Usando a API:

#### 1 - Salvar noticias:
#### Tipo de requisição:
    POST
#### Endpoint: 
    127.0.0.1:9000/noticia
#### Json:
        {
            "title_noticia": "Raptores",
            "text_noticia":"Dinossauros carnivores, velozes e mortais",
            "autor":"Pedrinho"
        }

#### 2 - Editar noticias:
#### Tipo de requisição:
    PUT
#### Endpoint ID:
    127.0.0.1:9000/noticia/1
#### Endpoint Titulo:
    127.0.0.1:9000/noticia/raptores
#### Json:
        {
            "title_noticia": "Raptores",
            "text_noticia":"Dinossauros carnivores, velozes e mortais"
        }

#### 3 - Deletar noticias:
#### Tipo de requisição:
    DELETE
#### Endpoint ID:
    127.0.0.1:9000/noticia/1
#### Endpoint Titulo:
    127.0.0.1:9000/noticia/raptores

#### 4 - Consultar noticias:
#### Tipo de requisição:
    GET
#### Endpoint Titulo:
    127.0.0.1:9000/noticia/raptores
#### Endpoint Texto:
    127.0.0.1:9000/noticia/Dinossauros carnivores, velozes e mortais
#### Endpoint Autor:
    127.0.0.1:9000/noticia/Pedrinho

#### 5 - Salvar Autor:
#### Tipo de requisição:
    POST
#### Endpoint: 
    127.0.0.1:9000/autor
#### Json:
        {
            "autor":"Pedrinho"
        }

#### 6 - Editar autor:
#### Tipo de requisição:
    PUT
#### Endpoint ID:
    127.0.0.1:9000/autor/1
#### Endpoint Titulo:
    127.0.0.1:9000/autor/Pedrinho
#### Json:
        {
            "name_autor":"Pedro"
        }

#### 7 - Deletar autor:
#### Tipo de requisição:
    DELETE
#### Endpoint ID:
    127.0.0.1:9000/autor/1
#### Endpoint Titulo:
    127.0.0.1:9000/autor/Pedro

#### 8 - Consultar autor:
#### Tipo de requisição:
    GET
#### Endpoint ID:
    127.0.0.1:9000/autor/1
#### Endpoint Nome:
    127.0.0.1:9000/autor/Pedro
#### Endpoint todos autores cadastrados:
    127.0.0.1:9000/autor

import json
import traceback
from flask import make_response
from models.tables import Autor, Noticia
from flask_restful import Resource, reqparse, request
from tools import logs, check_keys, service_error, mount_error, service_update_list_noticia

logger = logs()

class NoticiaRegister(Resource):
    def post(self):
        try:
            logger.info("Salvando dados da Noticia no banco...")

            data_request = request.get_json()
            result = check_keys(data_request)
            if result:
                service_error(f"Por gentileza, informe {' ,'.join(result)}", 410)

            parser = reqparse.RequestParser()
            parser.add_argument('id_noticia', type=int, required=False)
            parser.add_argument('title_noticia', type=str, required=True)
            parser.add_argument('text_noticia', type=str, required=True)
            parser.add_argument('autor', type=str, required=False)

            data = parser.parse_args()

            data_autor = AutorRegister.post(self, True)
            del data['autor']
            data.update(id_autor=data_autor.json['id_autor'])

            noticia = Noticia(**data)
            noticia.save_noticia()

            logger.info("Dados da noticia salvo com sucesso.")

            return make_response(noticia.jsonNoticia(), 200)

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

    def put(self, _id=False, _title=False):
        try:
            data = request.get_json()

            if _id:
                noticia = Noticia.find_by_id_notcia(_id)
                if not noticia:
                    service_error(f"Titulo({_id}) não encontrado", 411)

            if _title:
                noticia = Noticia.find_by_title(_title)
                if not noticia:
                    service_error(f"Titulo({_title}) não encontrado", 411)

            noticia.title_noticia = data['title_noticia']
            noticia.text_noticia = data['text_noticia']

            noticia.update_noticia()

            return make_response(noticia.jsonNoticia(), 200)

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

    def delete(self, _id=False, _title=False):
        try:
            if _id:
                noticia = Noticia.find_by_id_notcia(_id)
                if not noticia:
                    service_error(f"Titulo({_id}) não encontrado", 411)

                title = noticia.title_noticia

            if _title:
                noticia = Noticia.find_by_title(_title)
                if not noticia:
                    service_error(f"Titulo({_title}) não encontrado", 411)

                title = _title

            noticia.delete_noticia(noticia)

            return make_response(f"Noticia(titulo = {title}) excluida com sucesso", 200)

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

class AutorRegister(Resource):
    def post(self, service=False):
        try:
            logger.info("Salvando dados do autor no banco...")

            new_user = True

            data_request = request.get_json()
            check_autor_exist = AutorRegister.get(self, _name=data_request['autor'], service=True)

            if check_autor_exist is not None:
                new_user = False

            if new_user:
                if service:
                    save_autor = dict(name_autor=data_request['autor'])

                    if not 'id_autor' in data_request:
                        save_autor.update(id_autor=None)

                    data = save_autor

                else:
                    parser = reqparse.RequestParser()
                    parser.add_argument('id_autor', type=int, required=False)
                    parser.add_argument('name_autor', type=str, required=True)

                    data = parser.parse_args()

                autor = Autor(**data)
                autor.save_author()
                data_autor = autor.jsonAutor()

            elif service and not new_user:
                data_autor = check_autor_exist.jsonAutor()

            logger.info("Dados do autor salvo com sucesso.")

            return make_response(data_autor, 200)

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

    def get(self, _id=False, _name=False, service=False):
        try:
            if _id:
                autor = Autor.find_by_id(_id)
                if not autor:
                    service_error(f"Autor({_id}) não encontrado", 411)

                return make_response(autor.jsonAutor(), 200)

            if _name:
                autor = Autor.find_by_name(_name)
                if not service:
                    if not autor:
                        service_error(f"Autor({_name}) não encontrado", 411)

                    return make_response(autor.jsonAutor(), 200)

                else:
                    return autor

            autor = Autor.find_all()
            if not autor:
                service_error("Não foi encontrado nenhum registro", 412)

            return [autor.jsonAutor() for autor in autor], 200

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

    def put(self, _id=False, _name=False):
        try:
            data = request.get_json()

            if _id:
                autor = Autor.find_by_id(_id)
                if not autor:
                    service_error(f"Autor({_id}) não encontrado", 411)

            if _name:
                autor = Autor.find_by_name(_name)
                if not autor:
                    service_error(f"Autor({_name}) não encontrado", 411)

            autor.name_autor = data['name_autor']
            autor.update_author()

            return make_response(autor.jsonAutor(), 200)

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

    def delete(self, _id=False, _name=False):
        try:
            if _id:
                autor = Autor.find_by_id(_id)
                if not autor:
                    service_error(f"Autor({_id}) não encontrado", 411)

            if _name:
                autor = Autor.find_by_name(_name)
                if not autor:
                    service_error(f"Autor({_name}) não encontrado", 411)

            autor.delete_author(autor)

            return make_response("Autor excluido com sucesso", 200)

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

class Search_Noticia_Title(Resource):
    def get(self, _title):
        try:
            data_noticia = Noticia.find_by_title_all(_title)
            if not data_noticia:
                service_error(f"Titulo({_title}) não encontrado.", 413)

            noticia_list = service_update_list_noticia(data_noticia)

            return noticia_list, 200

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

class Search_Noticia_Text(Resource):
    def get(self, _text):
        try:
            data_noticia = Noticia.find_by_text(_text)
            if not data_noticia:
                service_error(f"Texto({_text}) não encontrado.", 414)

            noticia_list = service_update_list_noticia(data_noticia)

            return noticia_list, 200

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

class Search_Noticia_Autor(Resource):
    def get(self, _autor=False):
        try:
            data_autor = Autor.find_by_name(_autor)
            if not data_autor:
                service_error(f"Autor({_autor}) não encontrado", 411)

            noticia_list = service_update_list_noticia(data_autor.id_noticia, data_autor.name_autor)

            return noticia_list, 200

        except Exception as e:
            response = mount_error(e, traceback.print_exc())

            return response

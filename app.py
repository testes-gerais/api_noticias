from flask import Flask
from flask_cors import CORS
from flask_restful import Api
from settings import db, DB_URL
from sqlalchemy_utils import database_exists, create_database, drop_database

from resources.register import AutorRegister, NoticiaRegister, Search_Noticia_Title, Search_Noticia_Text,\
    Search_Noticia_Autor

app = Flask(__name__)
cors = CORS(app)

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

api = Api(app)
api.add_resource(AutorRegister, '/autor/', '/autor/<int:_id>', '/autor/<string:_name>', endpoint='autor')
api.add_resource(NoticiaRegister, '/noticia/', '/noticia/<int:_id>', '/noticia/<string:_title>', endpoint='noticia')
api.add_resource(Search_Noticia_Title, '/buscar_titulo/<string:_title>', endpoint='buscar_titulo')
api.add_resource(Search_Noticia_Text, '/buscar_texto/<string:_text>', endpoint='buscar_texto')
api.add_resource(Search_Noticia_Autor, '/buscar_autor/<int:_id>', '/buscar_autor/<string:_autor>',
                 endpoint='buscar_autor')

@app.before_first_request
def create_table():
    db.drop_all()
    db.create_all()

if __name__ == '__main__':
    db.init_app(app)
    app.run(debug=False, host='0.0.0.0', port=9000)

